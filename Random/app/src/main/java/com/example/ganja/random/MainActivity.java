package com.example.ganja.random;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    EditText et_min, et_max;
    Button buttonGenerate;
    TextView tv_result;
    int min,max,result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et_min = (EditText) findViewById(R.id.et_min);
        et_max = (EditText) findViewById(R.id.et_max);
        buttonGenerate = (Button) findViewById(R.id.buttonGenerate);
        tv_result = (TextView) findViewById(R.id.tv_Result);

        buttonGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tmpMin,tmpMax;
                tmpMax = et_max.getText().toString();
                tmpMin = et_min.getText().toString();

                if (!tmpMax.equals("") && !tmpMin.equals("")){
                    min = Integer.parseInt(tmpMin);
                    max = Integer.parseInt(tmpMax);
                    //tv_result.setText("wowo");

                    if (max>min){
                        result = rand(min,max);
                        tv_result.setText("" + result);
                    }

                }
            }
        });
    }

    public int rand(int min,int max){
        Random r = new Random();
        return r.nextInt((max-min) + 1) + min;
    }
}
