package main.java.com.ganjarm.tugaspcd;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Controller {
    @FXML
    private ImageView imageView_original, imageView_modified;
    @FXML
    private Button button_browsefile, button_negative, button_gamma, button_save;
    @FXML
    private Text text_browsefile;
    @FXML
    private TextField textField_gamma;

    private Image image_original;
    private WritableImage image_modified;
    private boolean allow_image_modification=false;
    private boolean is_image_modified=false;

    public void initialize(){
        button_browsefile.setOnAction(this::browseFile);
        button_negative.setOnAction(this::imageNegative);
        button_gamma.setOnAction(this::imageGamma);
        button_save.setOnAction(this::saveFile);
    }

    private void browseFile(ActionEvent event){
        //image chooser pop up
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter imageFilter = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png");
        fileChooser.getExtensionFilters().add(imageFilter);
        File file = fileChooser.showOpenDialog(stage);
        //check if any image is selected
        if(file!=null){
            allow_image_modification=true;
            //set file url text
            text_browsefile.setText(file.toURI().toString());
            //set original image
            this.image_original = new Image(file.toURI().toString());
            //set imageview
            imageView_original.setImage(this.image_original);
        }
    }

    private void imageNegative(ActionEvent event){
        if(allow_image_modification){
            //make pixelarray;
            PixelReader pixel_original = image_original.getPixelReader();
            //get widht and height from realimage
            int image_widht = (int) image_original.getWidth();
            int image_height = (int) image_original.getHeight();
            //make new image for modified pixel
            this.image_modified = new WritableImage(image_widht,image_height);
            PixelWriter pixel_modified = image_modified.getPixelWriter();
            //Negative Algorithm
            for(int x = 0;x < image_widht;x++){
                for(int y=0; y < image_height;y++){
                    //read pixel color[0 - 1.0]
                    Color original_color = pixel_original.getColor(x,y);
                    double original_red = original_color.getRed();
                    double original_green = original_color.getGreen();
                    double original_blue = original_color.getBlue();
                    //color = 1.0 - realcolor
                    Color negative_color = Color.color(1.0-original_red,1.0-original_green,1.0-original_blue,original_color.getOpacity());
                    //write pixel
                    pixel_modified.setColor(x,y,negative_color);
                }
            }
            //set modified image viewer
            this.imageView_modified.setImage(this.image_modified);
            this.is_image_modified=true;
        }
    }

    private void imageGamma(ActionEvent event){
        if(!textField_gamma.getText().isEmpty() && allow_image_modification){
            //read input from textfield
            double gamma = Double.parseDouble(textField_gamma.getText());
            //make gamma correction array
            int[] gamma_LUT = gammaLUT(gamma);
            //make pixelarray;
            PixelReader pixel_original = image_original.getPixelReader();
            //get widht and height from image
            int image_widht = (int) image_original.getWidth();
            int image_height = (int) image_original.getHeight();
            //make new image for modified pixel
            this.image_modified = new WritableImage(image_widht,image_height);
            PixelWriter pixel_modified = image_modified.getPixelWriter();
            //writing pixels
            for(int x = 0;x < image_widht;x++){
                for(int y=0; y < image_height;y++){
                    //read pixel color [0 - 255]
                    Color original_color = pixel_original.getColor(x,y);
                    int original_red = (int) (255*original_color.getRed());
                    int original_green = (int) (255*original_color.getGreen());
                    int original_blue = (int) (255*original_color.getBlue());
                    //make new color[0 - 1.0]
                    double modified_red = (double) gamma_LUT[original_red]/256;
                    double modified_green = (double) gamma_LUT[original_green]/256;
                    double modified_blue = (double) gamma_LUT[original_blue]/256;

                    Color gammaCor_color = Color.color(modified_red,modified_green,modified_blue,original_color.getOpacity());
                    //write pixel
                    pixel_modified.setColor(x,y,gammaCor_color);
                }
            }
            //set image viewer
            this.imageView_modified.setImage(image_modified);
            this.is_image_modified=true;

        }
    }

    private void saveFile(ActionEvent event){
        if(is_image_modified){
            Stage stage = new Stage();
            FileChooser fileChooser = new FileChooser();

            //Set extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
            fileChooser.getExtensionFilters().add(extFilter);

            //Show save file dialog
            File file = fileChooser.showSaveDialog(stage);

            if(file != null){
                System.out.println("masuk");
                try {
                    BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image_modified,null);
                    ImageIO.write(bufferedImage,"png",file);
                } catch (IOException e) {
                    System.out.println(e);
                }
            }
        }
    }

    private int[] gammaLUT(double gamma){
        int[] gamma_LUT = new int[256];
        for(int i = 0;i < 256;i++){
            gamma_LUT[i]= (int) (255*(Math.pow((double) i / (double) 255,gamma)));
        }

        return gamma_LUT;
    }

}
