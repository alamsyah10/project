package main.java.com.ganjarm.tugaspcd;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent root = fxmlLoader.load(getClass().getResource("main.fxml"));
        primaryStage.setTitle("PCD Tugas 1");
        primaryStage.setScene(new Scene(root, 1024, 800));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
