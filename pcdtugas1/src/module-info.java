module tugaspcd{
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.swing;
    requires javafx.base;
    requires javafx.graphics;

    opens main.java.com.ganjarm.tugaspcd to javafx.fxml;
    exports main.java.com.ganjarm.tugaspcd;

}