package diningphilosopher;

public class Philosopher implements Runnable {
    private Object leftFork;
    private Object rightFork;

    public Philosopher(Object leftFork, Object rightFork) {
        this.leftFork = leftFork;
        this.rightFork = rightFork;
    }

    @Override
    public void run(){
//        throw new UnsupportedOperationException("Not supported");
        while(true){
            try {
                //thinking
                doAction(System.nanoTime() + ": Thinking");
                //eating
                synchronized (leftFork){
                    doAction(System.nanoTime() + ": Picked up the leftFork");
                    synchronized (rightFork){
                        doAction(System.nanoTime() + ": Picked up the rightFork -> Eating");
                        doAction(System.nanoTime() + ": Put down rightFork");
                    }
                    doAction(System.nanoTime() + ": Put down leftFork");
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return;
            }
        }
    }

    private void doAction(String Action) throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + " " + Action );
        Thread.sleep((int)(Math.random()*10));
    }
}
