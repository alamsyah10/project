package diningphilosopher;

public class Diningphilosopher {
    public static void main(String[] Arg){
        Philosopher[] philosophers = new Philosopher[5];
        Object[] fork = new Object[philosophers.length];

        for (int i = 0; i < fork.length; i++){
            fork[i] = new Object();
        }
        for (int i=0;i < philosophers.length;i++){
            Object leftFork = fork[i];
            Object rightFork = fork[(i+1)%fork.length];

            if((i % 2) == 0)
                philosophers[i] = new Philosopher(leftFork,rightFork);
            else
                philosophers[i] = new Philosopher(rightFork,leftFork);

            Thread thread = new Thread(philosophers[i], "Philosopher "+ (i+1));
            thread.start();
        }
    }
}
