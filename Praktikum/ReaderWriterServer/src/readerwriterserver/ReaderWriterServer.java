package readerwriterserver;

public class ReaderWriterServer {
    public static void main(String args[]) {
        Database server = new Database();
        Reader[] readerArray = new Reader[NUM_OF_READERS];
        Writer[] writerArray = new Writer[NUM_OF_WRITERS];
        for (int i = 0; i < NUM_OF_READERS; i++) {
            readerArray[i] = new Reader(i, server);
            readerArray[i].start();
        }
        for (int i = 0; i < NUM_OF_WRITERS; i++) {
            writerArray[i] = new Writer(i, server);
            writerArray[i].start();
        }
    }

    private static final int NUM_OF_READERS = 3;
    private static final int NUM_OF_WRITERS = 2;
}

/*_______________________________________________________*/
/*class Reader___________________________________________*/
/*_______________________________________________________*/

class Reader extends Thread {
    public Reader(int r, Database db) {
        readerNum = r;
        server = db;
    }

    public void run() {
        int c;
        while (true) {
            Database.napping();
            System.out.println("reader " + readerNum +
                    " wants to read.");
            c = server.startRead();
            System.out.println("reader " + readerNum +
                    " is reading. Reader Count = " + c);
            Database.napping();
            System.out.print("reader " + readerNum +
                    " is done reading. ");
            c = server.endRead();
        }

    }

    private Database server;
    private int readerNum;
}

/*_______________________________________________________*/
/*class Writer___________________________________________*/
/*_______________________________________________________*/

class Writer extends Thread {
    public Writer(int w, Database db) {
        writerNum = w;
        server = db;
    }

    public void run() {
        while (true) {
            System.out.println("writer " + writerNum +
                    " is sleeping.");
            Database.napping();
            System.out.println("writer " + writerNum +
                    " wants to write.");
            server.startWrite();luprus linux
            System.out.println("writer " + writerNum +
                    " is writing.");
            Database.napping();
            System.out.println("writer " + writerNum +
                    " is done writing.");
            server.endWrite();
        }
    }

    private Database server;
    private int writerNum;
}

/*_______________________________________________________*/
/*class Semaphore________________________________________*/
/*_______________________________________________________*/

final class Semaphore {
    public Semaphore() {
        value = 0;
    }

    public Semaphore(int v) {
        value = v;
    }

    public synchronized void P() {
        while (value <= 0) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        value--;
    }

    public synchronized void V() {
        ++value;
        notify();
    }

    private int value;
}

/*_______________________________________________________*/
/*class Database_________________________________________*/
/*_______________________________________________________*/

class Database {
    public Database() {
        readerCount = 0;
        mutex = new Semaphore(1);
        db = new Semaphore(1);
    }

    public static void napping() {
        int sleepTime = (int) (NAP_TIME * Math.random());
        try {
            Thread.sleep(sleepTime * 1000);
        } catch (InterruptedException e) {
        }
    }

    public int startRead() {
        mutex.P();
        ++readerCount;
        if (readerCount == 1) {
            db.P();
        }
        mutex.V();
        return readerCount;
    }

    public int endRead() {
        mutex.P();
        --readerCount;
        if (readerCount == 0) {
            db.V();
            ;
        }
        mutex.V();
        System.out.println("Reader count = " + readerCount);
        return readerCount;
    }

    public void startWrite() {
        db.P();
    }

    public void endWrite() {
        db.V();
    }

    private int readerCount;
    Semaphore mutex;
    Semaphore db;
    private static final int NAP_TIME = 15;
}