//USING EXPRESS
var express = require('express');
var app = express();
//OtherJS
require('./chatAPI')(app);

//ROUTING
app.get('/',function (req, res) {
  res.send('Root');
});

app.get('/admin',function (req, res) {
  res.send('Admin');
});
//SERVER
var server = app.listen(8081,function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log("Server listening at http://%s:%s", host, port);
});