
public class Main {
	public static void main(String[] arg) {
		Graph graph = new Graph(5);
		graph.addEdge(graph, 0, 1);
		graph.addEdge(graph, 1, 2);
		graph.addEdge(graph, 1, 4);
		graph.addEdge(graph, 2, 3);
		
		BFS Obj = new BFS(graph, 2);
		//DFS Obj = new DFS(graph, 2);
		Obj.run();
		
	}
}