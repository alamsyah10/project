import java.util.LinkedList;
public class Graph {
	int vertexSize;
    LinkedList<Integer> adjListArray[];

    // constructor 
    Graph(int vertexSize) {
        this.vertexSize = vertexSize;

        adjListArray = new LinkedList[vertexSize];

        for (int i = 0; i < vertexSize; i++) {
            adjListArray[i] = new LinkedList<>();
        }
    }
    void addEdge(Graph graph, int src, int dest) {
        // Add an edge from src to dest. 
        graph.adjListArray[src].addFirst(dest);
 
        // Since graph is undirected, add an edge from dest
        // to src also
        graph.adjListArray[dest].addFirst(src);
    }
 
    void printGraph(Graph graph) {
        for (int v = 0; v < graph.vertexSize; v++) {
            System.out.println("Adjacency list of vertex " + v);
            System.out.print("head");
            for (Integer pCrawl : graph.adjListArray[v]) {
                System.out.print(" -> " + pCrawl);
            }
            System.out.println("\n");
        }
    }
}
