import java.util.LinkedList;;
public class BFS {
	boolean visited[];
	int start;
	Graph graph;
	LinkedList<Integer> queue = new LinkedList<Integer>();
	BFS(Graph graph, int start) {
		this.graph=graph;
		this.start=start;
		visited = new boolean[graph.vertexSize];
	}
	public void run() {
		int position;
		queue.add(start);
		visited[start]=true;
		System.out.print("Visit : ");
		while(!queue.isEmpty()) {
			position = queue.pollFirst();
			System.out.print(position + " ");
			for (Integer i : graph.adjListArray[position]) {
				if (!visited[i]) {
					visited[i]=true;
					queue.add(i);
				}
			}
		}
	}
}
