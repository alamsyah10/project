var fs = require('fs');
var dataset_path = "../assets/dataset/cardio_train.json";
var dataset_json = JSON.parse(fs.readFileSync(dataset_path));

var train_range = 10000;
var test_range = 1000;
var max_epoch = 200;
var learning_rate = 0.00001;

var initial_weight; //[0] false, [1] true
initial_weight = [{
    'age' : 10950 , //age at 30
    'gender' : 0, //male
    'height' : 175, //175 cm
    'weight' : 80,
    'ap_hi' : 120,
    'ap_lo' : 90,
    'cholesterol' : 1,
    'gluc' : 0,
    'smoke' : 0,
    'alco' : 0,
    'active' : 1
},
    {
    'age' : 20075 , //age at 55
    'gender' : 1, //female
    'height' : 165, //165 cm
    'weight' : 85,
    'ap_hi' : 140,
    'ap_lo' : 90,
    'cholesterol' : 3,
    'gluc' : 1,
    'smoke' : 0,
    'alco' : 0,
    'active' : 1
}];

//change weight to array
var weight = [
    [
        initial_weight[0].age,
        initial_weight[0].gender,
        initial_weight[0].height,
        initial_weight[0].weight,
        initial_weight[0].ap_hi,
        initial_weight[0].ap_lo,
        initial_weight[0].cholesterol,
        initial_weight[0].gluc,
        initial_weight[0].smoke,
        initial_weight[0].alco,
        initial_weight[0].active
    ],
    [
        initial_weight[1].age,
        initial_weight[1].gender,
        initial_weight[1].height,
        initial_weight[1].weight,
        initial_weight[1].ap_hi,
        initial_weight[1].ap_lo,
        initial_weight[1].cholesterol,
        initial_weight[1].gluc,
        initial_weight[1].smoke,
        initial_weight[1].alco,
        initial_weight[1].active,
    ]
];

//change dataset to array
var dataset_train = []; //0 age, 1 gender, 2 height, 3 weight, 4 ap_hi, 5 ap_lo, 6 cholesterol, 7 gluc, 8 smoke, 9 alco, 10 active, 11 cardio/target
for (i=0;i<train_range+test_range;i++) {
    dataset_train.push([
        dataset_json[i].age,
        dataset_json[i].gender,
        dataset_json[i].height,
        dataset_json[i].weight,
        dataset_json[i].ap_hi,
        dataset_json[i].ap_lo,
        dataset_json[i].cholesterol,
        dataset_json[i].gluc,
        dataset_json[i].smoke,
        dataset_json[i].alco,
        dataset_json[i].active,
        dataset_json[i].cardio]
    );
}

//adjust weight
function adjustWeight(prediction,condition,index) {
    if(condition){
        for (var i=0;i<11;i++){
            weight[prediction][i] += learning_rate*dataset_train[index][i];
        }
    }
    else {
        for (var i=0;i<11;i++){
            weight[prediction][i] -= learning_rate*dataset_train[index][i];
        }
    }

}

//train function
function train() {
    for(var i=0;i<train_range;i++){
     //distance
        //sum class 0
        var sum_0 = 0;
        for(var j=0;j<11;j++)
            sum_0+=(dataset_train[i][j]-weight[0][j])*(dataset_train[i][j]-weight[0][j]);
        //sum class 1
        var sum_1 = 0;
        for(var j=0;j<11;j++)
            sum_1+=(dataset_train[i][j]-weight[1][j])*(dataset_train[i][j]-weight[1][j]);
        // console.log(sum_0);
        // console.log(sum_1);
        // console.log(dataset_train[i][11]);
        // console.log("--------------");
        if (sum_0<sum_1){
            if(dataset_train[i][11]==0)
                adjustWeight(0,true,i);
            else
                adjustWeight(0,false,i);
        }
        else {
            if(dataset_train[i][11]==1)
                adjustWeight(1,true,i);
            else
                adjustWeight(1,false,i)
        }

    }

}

function predict() {
    var count=0;
    for (var i = train_range; i < train_range+test_range; i++) {
        //distance
        //sum class 0
        var sum_a = 0;
        for (var j = 0; j < 11; j++)
            sum_a += (dataset_train[i][j] - weight[0][j]) * (dataset_train[i][j] - weight[0][j]);
        //sum class 1
        var sum_b = 0
        for (var j = 0; j < 11; j++)
            sum_b += (dataset_train[i][j] - weight[1][j]) * (dataset_train[i][j] - weight[1][j]);

        if (sum_a < sum_b) {
            if (dataset_train[i][11] == 0)
                count++;
        } else {
            if (dataset_train[i][11] == 1)
                count++
        }

        // console.log(sum_a);
        // console.log(sum_b);
        // console.log(dataset_train[i][11]);
        // console.log("--------------");
    }
    console.log(count);
}

//run
var start_time = new Date().getTime();
for(var i=0;i<max_epoch;i++)
    train();

console.log(weight);
console.log(start_time-new Date().getTime());
predict();
