var fs = require('fs');
var dataset_path = "../assets/dataset/cardio_train.json"
var dataset_file = fs.readFileSync(dataset_path);
var dataset = JSON.parse(dataset_file);

var initial_weight;
initial_weight = [{
    'age' : 10950 , //age at 30
    'gender' : 0, //male
    'height' : 175, //175 cm
    'weight' : 80,
    'ap_hi' : 120,
    'ap_lo' : 90,
    'cholesterol' : 1,
    'gluc' : 0,
    'smoke' : 0,
    'alco' : 0,
    'active' : 1,
    'cardio' : 0 //target
},
    {
        'age' : 20075 , //age at 55
        'gender' : 1, //female
        'height' : 165, //165 cm
        'weight' : 85,
        'ap_hi' : 140,
        'ap_lo' : 90,
        'cholesterol' : 3,
        'gluc' : 1,
        'smoke' : 0,
        'alco' : 0,
        'active' : 1,
        'cardio' : 1 //target
    }];

console.log(initial_weight[0] - dataset[1]);



