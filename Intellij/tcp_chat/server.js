var net = require("net");
var fs = require('fs');

// ip host & port for connection
var HOST = '127.0.0.1';
var PORT = 6969;

var clientUsername = [];
var clientSocket = [];
var database = require('./database.json');
var server = net.createServer(function (socket) {
    console.log(Date()+":["+socket.remoteAddress+":"+socket.remotePort+" connected]");
    socket.on('data', function (data) {
	    var String_data = data.toString();
	    String_data = String_data.replace(/(\r\n|\n|\r)/gm,"");
        var res = String_data.split(" ");
        var socketlen = clientSocket.length;
        var reslen = res.length;
        console.log(Date()+":[stream from "+socket.remoteAddress+":"+socket.remotePort+"] | for debugging -->");
        for (i=0;i<reslen;i++){
            console.log(i+":"+res[i]+";");
        }

        if (reslen < 1){
            socket.write("/status 3");
            return;
        }
        var string ="";
        var i; //indexing
        var username=clientUsername[clientSocket.indexOf(socket)];
        var password;

        //register
        if (res[0] === "/register"){
            //checking syntax
            if (reslen < 3){
                socket.write("/status 3");
                return;
            }
            if (res[1].length === 0 && res[2].length === 0){
                socket.write("/status 3");
                return;
            }
            username = res[1].toString();
            password = res[2].toString();
            //checking used username
            for (i = 0; i < database.users.length; i++) {
                if (database.users[i].username === username) {
                    console.log(Date()+":["+socket.remoteAddress + ":" + socket.remotePort + " register with used username]");
                    socket.write("/status 5");
                    return;
                }
            }
            //adding to database
            var users = {
                username : username,
                password : password
            };
            database.users.push(users);
            fs.writeFileSync('database.json',JSON.stringify(database));
            console.log(Date()+":["+socket.remoteAddress + ":" + socket.remotePort + " register using username : "+username+"]");
            socket.write("/status 4");
        }
        //login
        else if (res[0]=== "/login"){
            //checking syntax
            if (reslen < 3){
                socket.write("/status 3");
                return;
            }
            if (res[1].length === 0 && res[2].length === 0){
                socket.write("/status 3");
                return;
            }
            //login
            var loggedIn = false;
            username = res[1].toString();
            password = res[2].toString();
            for (i = 0; i < database.users.length; i++) {
                if (database.users[i].username === username && database.users[i].password === password) {
                    clientUsername.push(username);
                    clientSocket.push(socket);
                    loggedIn = true;
                    console.log(Date()+":["+socket.remoteAddress + ":" + socket.remotePort + " logged in using username : "+username+"]");
                    socket.write("/status 4");
                    break;
                }
            }
            //false username or password
            if (loggedIn === false)
                socket.write("/status 5");
        }
        //listing users
        else if (res[0].toString()==="/list"){
            console.log(Date()+":["+socket.remoteAddress + ":" + socket.remotePort +" "+ username + " accessing online list]");
            var list = "";
            if (clientUsername.length === 0)
                socket.write("/status 2");
            else{
                for (i = 0; i<clientUsername.length;i++)
                    list += " " + clientUsername[i];
                socket.write("/users"+list);
            }
        }
        //broadcast
        else if (res[0] === "/broadcast"){
            //checking user
            if (reslen < 2){
                socket.write("/status 3");
                return;
            }
            if (typeof username === 'undefined'){
                socket.write("/status 4");
                return;
            }
            if (clientUsername.length <= 1){
                socket.write("/status 2");
                console.log(Date()+":["+socket.remoteAddress + ":" + socket.remotePort +" "+ username + " broadcasting error, no online users]");
                return;
            }
            //string from client
            for (i=1;i<reslen;i++)
                string += " "+res[i];
            //send broadcast
            for (i=0;i<socketlen;i++){
                if (clientSocket[i]!==socket){
                    clientSocket[i].write("/msg " + username + " " + string)
                }
            }
            socket.write("/status 1");
            console.log(Date()+":["+socket.remoteAddress + ":" + socket.remotePort +" "+ username + " broadcasting success]");
        }
        //chat
        else if (res[0]==="/chat") {
            //checking syntax
            if (reslen < 3){
                socket.write("/status 3");
                return;
            }
            if (res[1].length === 0 && res[2].length === 0){
                socket.write("/status 3");
                return
            }
            //checking user
            if (typeof username === 'undefined'){
                socket.write("/status 4");
                return;
            }

            var receiver=clientUsername.indexOf(res[1].toString());
            if (receiver!==-1){
                for (i = 2; i < reslen; i++)
                    string += " " + res[i];
                clientSocket[receiver].write("/msg "+username + string);
                console.log(Date()+":["+socket.remoteAddress + ":" + socket.remotePort +" "+ username + " chat to "+ res[1] +" success]");
                socket.write("/status 1");
            }
            else{
                socket.write("/status 2");
                console.log(Date()+":["+socket.remoteAddress + ":" + socket.remotePort +" "+ username + " chat to "+ receiver + " declined. Receiver missing or unregistered");
            }
        }
        //response other syntax
        else
            socket.write("/status 3");
    });
    //deleting unused socket
    socket.on('end', function () {
        console.log(Date()+":["+socket.remoteAddress + ":" + socket.remotePort + " disconnected]");
        var id = clientSocket.indexOf(socket);
        if(id !== -1) {
            console.log(Date()+":["+clientUsername[id] + " disconnected]");
            delete clientSocket[id];
            delete clientUsername[id];
        }
    });
    //throw error event from client connection
    socket.on('error', function (err) {
        console.log(Date()+':['+err +']');
        console.log(Date()+":["+socket.remoteAddress + ":" + socket.remotePort + " force disconnected]");
        var id = clientSocket.indexOf(socket);
        if(id !== -1) {
            console.log(Date()+":["+clientUsername[id] + " force disconnected]");
            delete clientSocket[id];
            delete clientUsername[id];
        }
    });
});

server.listen(PORT,HOST);
console.log(Date()+":[server created at "+HOST+":"+PORT+"]");
