package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.*;
import javafx.scene.paint.Color;
import static java.lang.Integer.parseInt;


public class Controller {
    @FXML
    ImageView iv_hasil;
    Image gambar;
    @FXML
    Button btn_go;
    @FXML
    TextField tf_level;

    int width = 700;
    int height = 700;

    public void initialize(){
        this.gambar = new Image(getClass().getResourceAsStream("/sample/sqwd.jpg"), 700, 700, false, false);
        btn_go.setOnAction(this::Go);
        iv_hasil.setImage(this.gambar);
    }
    private void Go(ActionEvent event){
        int lvl= parseInt(tf_level.getText());
        affine hmmm = new affine(gambar,height,width);
        hmmm.setLvl(lvl);
        iv_hasil.setImage(hmmm.Draw());
    }
}

class affine {
    int width = 700;
    int height = 700;
    Image gambar;
    private int lvl;

    public affine(Image gambar, int width, int height) {
        this.width = width;
        this.height = height;
        this.gambar = gambar;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    //gambar 1
        int gbr1_x1= (int) ((float)width/4);
        int gbr1_y1=0;
        int gbr1_x2= (int) (3*(float)width/4);
        int gbr1_y2= (int) ((float)height/2);
    //gambar 2
        int gbr2_x1=0;
        int gbr2_y1=(int) ((float)height/2);
        int gbr2_x2=(int) ((float)width/2);
        int gbr2_y2=height;
    //gambar 3
        int gbr3_x1=(int) ((float)width/2);
        int gbr3_y1=(int) ((float)height/2);
        int gbr3_x2=width;
        int gbr3_y2=height;

    private WritableImage Resize(PixelReader reader){
        WritableImage writableImage = new WritableImage(width/2,height/2);
        PixelWriter pixelWriter=writableImage.getPixelWriter();

        for(int x=0;x<width;x++){
            for (int y=0;y<height;y++){
                Color color = reader.getColor(x,y);
                pixelWriter.setColor(x/2,y/2,color);
            }
        }
        return  writableImage;
    }

    public WritableImage Draw(){
        PixelReader reader = this.gambar.getPixelReader();
        WritableImage srcGambar = Resize(reader);
        WritableImage destGambar = new WritableImage(width,height);
        destGambar.getPixelWriter();
        PixelWriter pixelWriter;
        for (int i=lvl; i>0;i--){
            PixelReader pixelReader = srcGambar.getPixelReader();
            destGambar = new WritableImage(width,height);
            pixelWriter = destGambar.getPixelWriter();
            for (int x = gbr1_x1;x<gbr1_x2;x++){
                for (int y = gbr1_y1;y<gbr1_y2;y++){
                    Color color = pixelReader.getColor(x-gbr1_x1, y-gbr1_y1);
                    pixelWriter.setColor(x, y, color);
                }
            }
            for (int x = gbr2_x1;x<gbr2_x2;x++){
                for (int y = gbr2_y1;y<gbr2_y2;y++){
                    Color color = pixelReader.getColor(x-gbr2_x1, y-gbr2_y1);
                    pixelWriter.setColor(x, y, color);
                }
            }
            for (int x = gbr3_x1;x<gbr3_x2;x++){
                for (int y = gbr3_y1;y<gbr3_y2;y++){
                    Color color = pixelReader.getColor(x-gbr3_x1, y-gbr3_y1);
                    pixelWriter.setColor(x, y, color);
                }
            }
            reader = destGambar.getPixelReader();
            srcGambar = Resize(reader);
        }
        WritableImage hasil = new WritableImage(reader,width,height);
        return hasil;
    }

}
