var express = require('express');
var router = express();
const path = require('path')

//get api
router.get('/',function (req, res, next) {
    console.log(path.join(__dirname, '../public', 'api.html'));
    res.sendFile(path.join(__dirname, '../public', 'api.html'));
});

module.exports = router;