package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.SubScene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.animation.*;
import javafx.util.Duration;
import javafx.scene.Group;

public class Controller {
    Canvas canvas;
    @FXML
    Button btn1;
    @FXML
    SubScene scene_main;
    @FXML
    ImageView ivWallpaper;
    float power =80;
    public void initialize(){
        setWallpaper();
        btn1.setOnAction(this::fire);
    }

    private void setWallpaper(){
        Image imageWall = new Image(getClass().getResourceAsStream("/FiringSquirel.png"));
        ivWallpaper.setImage(imageWall);
    }
    private void fire(ActionEvent event){
//        Image ammo = new Image(getClass().getResourceAsStream("/ammo.png"));
//        ImageView ivammo = new ImageView();
//        ivammo.setImage(ammo);
//        ivammo.setX(269);
//        ivammo.setY(521);
//
//        RotateTransition rotateTransition = new RotateTransition();
//
//        //Setting the duration for the transition
//        rotateTransition.setDuration(Duration.millis(1000));
//
//        //Setting the node for the transition
//        rotateTransition.setNode(ivammo);
//
//        //Setting the angle of the rotation
//        rotateTransition.setByAngle(360);
//
//        //Setting the cycle count for the transition
//        rotateTransition.setCycleCount(50);
//
//        //Setting auto reverse value to false
//        rotateTransition.setAutoReverse(false);
//
//        //Playing the animation
//        rotateTransition.play();
//        scene_main.setFill(Color.RED);
//        scene_main.setClip(ivammo);

        Circle circle = new Circle();
        circle.setCenterX(240);
        circle.setCenterY(540);
        circle.setRadius(30);
        circle.setFill(Color.BROWN);
        TranslateTransition translateTransition = new TranslateTransition();
        Group root = new Group(circle);
        //trans
        float transX = 768*power/10000;
        float transY = -768;
        for (int i = 0; i<10;i++){
          //  translateTransition.setDuration(Duration.millis(1000));
            transY = -768*power/10000;
            translateTransition.setDuration(Duration.millis(1000));

            System.out.println(transY);
            translateTransition.setNode(circle);
            translateTransition.setByX(transX);
            translateTransition.setByY(transY);
            translateTransition.setAutoReverse(false);
            translateTransition.play();
            root = new Group(circle);
            scene_main.setRoot(root);
        }
        //Setting the duration of the transition
//        translateTransition.setDuration(Duration.millis(1000));
//
//        //Setting the node for the transition
//        translateTransition.setNode(circle);
//
//        //Setting the value of the transition along the x axis.
//        translateTransition.setByX(768);
//        translateTransition.setByY(-768);
//        translateTransition.setAutoReverse(false);

        //Playing the animation
        translateTransition.play();
        //Group root = new Group(circle);
        scene_main.setRoot(root);
    }
    private void animationl(ActionEvent event){
        Image plane =  new Image(getClass().getResourceAsStream("/png-hd-airplane-flight-image-free-png-in-sky-plane-hd-png-709.png"));
        ImageView ivplane = new ImageView();
        ivplane.setImage(plane);


        //Creating a rotate transition
        RotateTransition rotateTransition = new RotateTransition();

        //Setting the duration for the transition
        rotateTransition.setDuration(Duration.millis(1000));

        //Setting the node for the transition
        rotateTransition.setNode(ivplane);

        //Setting the angle of the rotation
        rotateTransition.setByAngle(360);

        //Setting the cycle count for the transition
        rotateTransition.setCycleCount(50);

        //Setting auto reverse value to false
        rotateTransition.setAutoReverse(false);

        //Playing the animation
        rotateTransition.play();


//        scene_main.setRoot(ivplane);
        scene_main.setClip(ivplane);
    }
}
