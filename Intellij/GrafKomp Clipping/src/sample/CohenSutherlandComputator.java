package sample;
class CohenSutherlandComputator {

    // Area codes
    private static final byte INSIDE = 0; // 0000
    private static final byte LEFT = 1; // 0001
    private static final byte RIGHT = 2; // 0010
    private static final byte BOTTOM = 4; // 0100
    private static final byte TOP = 8; // 1000

    float[] points = new float[4];

    private static byte computeCode(float[] clipSize, float x, float y) {

        byte code = INSIDE;

        if (x < clipSize[0]) {
            code |= LEFT;
        } else if (x > clipSize[2]) {
            code |= RIGHT;
        }
        if (y < clipSize[1]) {
            code |= TOP;
        } else if (y > clipSize[3]) {
            code |= BOTTOM;
        }

        return code;
    }

    boolean clipLine(float[] clipSize, float[] points) {
        if (points.length != 4) {
            throw new IllegalArgumentException("Array Error");
        }

        float x1 = points[0];
        float y1 = points[1];
        float x2 = points[2];
        float y2 = points[3];

        byte code1 = computeCode(clipSize, x1, y1);
        byte code2 = computeCode(clipSize, x2, y2);

        boolean isClipped = false;

        while (true) {
            if ((code1 | code2) == INSIDE) {
                isClipped = true;
                break;
            } else if ((code1 & code2) != INSIDE) {
                break;
            } else {
                final float clipX;
                final float clipY;

                final byte outCode = (code1 != INSIDE) ? code1 : code2;

                if ((outCode & TOP) != INSIDE) {
                    clipX = x1 + (x2 - x1) * (clipSize[1] - y1) / (y2 - y1);
                    clipY = clipSize[1];
                } else if ((outCode & BOTTOM) != INSIDE) {
                    clipX = x1 + (x2 - x1) * (clipSize[3] - y1) / (y2 - y1);
                    clipY = clipSize[3];
                } else if ((outCode & RIGHT) != INSIDE) {
                    clipY = y1 + (y2 - y1) * (clipSize[2] - x1) / (x2 - x1);
                    clipX = clipSize[2];
                } else if ((outCode & LEFT) != INSIDE) {
                    clipY = y1 + (y2 - y1) * (clipSize[0] - x1) / (x2 - x1);
                    clipX = clipSize[0];
                } else {
                    clipX = Float.NaN;
                    clipY = Float.NaN;
                }
                if (outCode == code1) {
                    x1 = clipX;
                    y1 = clipY;
                    code1 = computeCode(clipSize, x1, y1);
                } else {
                    x2 = clipX;
                    y2 = clipY;
                    code2 = computeCode(clipSize, x2, y2);
                }
            }
        }
        points[0] = x1;
        points[1] = y1;
        points[2] = x2;
        points[3] = y2;
        System.arraycopy(points, 0, this.points, 0, 4);
        return isClipped;
    }
}
