package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

public class Controller {
    @FXML
    Canvas cvsWindow,cvsViewPort;
    @FXML
    Button btnSetWindow,btnDrawLine,btnReset;
    @FXML
    TextField tfWindowStartX, tfWindowStartY,tfWindowEndY,tfWindowEndX;
    @FXML
    TextField tfLineStartY,tfLineStartX,tfLineEndX,tfLineEndY;

    private float[] clipSize = new float[4];
    private float[] points = new float[4];
    // 0 = startX, 1 = startY, 2 = endX, 3 = endY

    public void initialize(){
        btnSetWindow.setOnAction(this::setWindow);
        btnDrawLine.setOnAction(this::Draw);
        btnReset.setOnAction(this::Reset);
    }

    private void setWindow(ActionEvent event){
        clipSize[0] = Float.parseFloat(tfWindowStartX.getText());
        clipSize[1] = Float.parseFloat(tfWindowStartX.getText());
        clipSize[2] = Float.parseFloat(tfWindowEndX.getText());
        clipSize[3] = Float.parseFloat(tfWindowEndY.getText());
        drawSide();
    }
    private void Draw(ActionEvent event){
        setLine();
        drawViewPort();
        setClip();
        drawWindow();
    }
    private void Reset(ActionEvent event){
        GraphicsContext gcViewPort = cvsViewPort.getGraphicsContext2D();
        GraphicsContext gcWindow = cvsWindow.getGraphicsContext2D();
        gcViewPort.clearRect(0,0,cvsViewPort.getWidth(),cvsViewPort.getHeight());
        gcWindow.clearRect(0,0,cvsWindow.getWidth(),cvsWindow.getHeight());
    }
    private void setLine(){
        points[0] = Float.parseFloat(tfLineStartX.getText());
        points[1] = Float.parseFloat(tfLineStartX.getText());
        points[2] = Float.parseFloat(tfLineEndX.getText());
        points[3] = Float.parseFloat(tfLineEndY.getText());
    }
    private void setClip(){
        CohenSutherlandComputator clip = new CohenSutherlandComputator();
        System.out.println(clip.clipLine(clipSize,points));
        for(int i = 0;i<4;i++){
            this.points[i] = clip.points[i];
            System.out.println(i +":" + this.points[i]);
        }
    }
    private void drawSide(){
        GraphicsContext gcViewPort = cvsViewPort.getGraphicsContext2D();
        GraphicsContext gcWindow = cvsWindow.getGraphicsContext2D();

        //viewport
        gcViewPort.setLineWidth(1.0);
        gcViewPort.setStroke(Color.RED);
        //top to bottom
        gcViewPort.moveTo(clipSize[0],0);
        gcViewPort.lineTo(clipSize[0],cvsViewPort.getHeight());
        gcViewPort.moveTo(clipSize[2],0);
        gcViewPort.lineTo(clipSize[2],cvsViewPort.getHeight());
        //left to right
        gcViewPort.moveTo(0,clipSize[1]);
        gcViewPort.lineTo(cvsViewPort.getWidth(),clipSize[1]);
        gcViewPort.moveTo(0,clipSize[3]);
        gcViewPort.lineTo(cvsViewPort.getWidth(),clipSize[3]);
        gcViewPort.stroke();

        //window
        gcWindow.setLineWidth(1.0);
        gcWindow.setStroke(Color.RED);
        //top to bottom
        gcWindow.moveTo(clipSize[0],0);
        gcWindow.lineTo(clipSize[0],cvsWindow.getHeight());
        gcWindow.moveTo(clipSize[2],0);
        gcWindow.lineTo(clipSize[2],cvsWindow.getHeight());
        //left to right
        gcWindow.moveTo(0,clipSize[1]);
        gcWindow.lineTo(cvsWindow.getWidth(),clipSize[1]);
        gcWindow.moveTo(0,clipSize[3]);
        gcWindow.lineTo(cvsWindow.getWidth(),clipSize[3]);
        gcWindow.stroke();

    }
    private void drawViewPort(){
        GraphicsContext gcViewPort = cvsViewPort.getGraphicsContext2D();
        gcViewPort.setLineWidth(1.0);
        gcViewPort.setStroke(Color.BLACK);
        gcViewPort.moveTo(points[0],points[1]);
        gcViewPort.lineTo(points[2],points[3]);
        gcViewPort.stroke();
    }
    private void drawWindow(){
        GraphicsContext gcWindow = cvsWindow.getGraphicsContext2D();
        gcWindow.setLineWidth(1.0);
        gcWindow.setStroke(Color.BLACK);
        gcWindow.moveTo(points[0],points[1]);
        gcWindow.lineTo(points[2],points[3]);
        gcWindow.stroke();
    }



}
