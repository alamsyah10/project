package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

import java.io.File;


import static java.lang.Integer.parseInt;

public class Controller {
    @FXML
    private TextField InputX1,InputY1,InputX2,InputY2,InputR,Widht,Height;
    @FXML
    private Button BtnMake;
    @FXML
    private ImageView imageView;
    public void initialize(){
        System.out.println("lol");
        //FileInputStream file = new FileInputStream("D:/Workspace/Intellij/Line2D_Javafx/src/image/Izumi.Sagiri.full.2090331.jpg");
        File file = new File("src/image/Izumi.Sagiri.full.2090331.jpg");
        Image imagestart = new Image(file.toURI().toString());
        imageView.setImage(imagestart);
        BtnMake.setOnAction(// imageView.setImage(Image.fromPlatformImage(line));
                this::handle);

    }


    private void handle(ActionEvent event) {
        System.out.println("button ok");
        int X1 = parseInt(InputX1.getText());
        int X2 = parseInt(InputX2.getText());
        int Y1 = parseInt(InputY1.getText());
        int Y2 = parseInt(InputY2.getText());
        int R = parseInt(InputR.getText());
        int imgWidth = parseInt(Widht.getText());
        int imgHeight = parseInt(Height.getText());
        WritableImage img = new WritableImage(imgWidth, imgHeight);

        System.out.println("(" + X1 + "," + Y1 + "), (" + X2 + "," + Y2 + ")");
        Line line = new Line(X1, Y1, X2, Y2);
        line.setStroke(Color.BLACK);
    }

}
