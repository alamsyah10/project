package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.io.File;

public class Controller {
    @FXML
    private ImageView imageView;
    @FXML
    private Button btn_line,btn_circle;
    @FXML
    private TextField tf_x1,tf_y1,tf_x2,tf_y2,tf_xCircle,tf_yCircle,tf_rad;
    @FXML
    private Label lbl_error;
    public void initialize(){
        File imgfile = new File("src/image/Izumi.Sagiri.full.2090331.png");
        Image imagestart = new Image(imgfile.toURI().toString());
        imageView.setImage(imagestart);
        btn_line.setOnAction(this::makeLine);
        btn_circle.setOnAction(this::makecircle);
    }
    private void makeLine(ActionEvent event){
        imageView.setImage(null);
        lbl_error.setText(null);
        int x1 = Integer.parseInt(tf_x1.getText());
        int y1 = Integer.parseInt(tf_y1.getText());
        int x2 = Integer.parseInt(tf_x2.getText());
        int y2 = Integer.parseInt(tf_y2.getText());

        if (x1>=700 || x2>=700 || y1>=700 || y2>=700)
            lbl_error.setText("Out of bound");
        else{
            WritableImage writableImage = new WritableImage(700,700);
            PixelWriter pixelWriter = writableImage.getPixelWriter();
            int dx = x2-x1;
            int dy = y2-y1;
            int y;
            for (int x=x1;x<=x2;x++){
                y=y1+dy*(x-x1)/dx;
                pixelWriter.setColor(x,y, Color.BLACK);
                imageView.setImage(writableImage);
            }
        }
    }
    private void makecircle(ActionEvent event){
        imageView.setImage(null);
        lbl_error.setText(null);
        int x0 = Integer.parseInt(tf_xCircle.getText());
        int y0 = Integer.parseInt(tf_yCircle.getText());
        int rad = Integer.parseInt(tf_rad.getText());

        if (x0+rad>=700 || y0+rad>=700)
            lbl_error.setText("Out of bound");
        else{
            WritableImage writableImage = new WritableImage(700,700);
            PixelWriter pixelWriter = writableImage.getPixelWriter();
            int x = rad -1;
            int y = 0;
            int dx = 1;
            int dy = 1;
            int err = dx - (rad<<1);

            while (x>=y){
                pixelWriter.setColor(x0 + x, y0 + y, Color.BLACK);
                pixelWriter.setColor(x0 + y, y0 + x, Color.BLACK);
                pixelWriter.setColor(x0 - y, y0 + x, Color.BLACK);
                pixelWriter.setColor(x0 - x, y0 + y, Color.BLACK);
                pixelWriter.setColor(x0 - x, y0 - y, Color.BLACK);
                pixelWriter.setColor(x0 - y, y0 - x, Color.BLACK);
                pixelWriter.setColor(x0 + y, y0 - x, Color.BLACK);
                pixelWriter.setColor(x0 + x, y0 - y, Color.BLACK);

                if (err<=0){
                    y++;
                    err += dy;
                    dy += 2;
                }
                else {
                    x--;
                    dx += 2;
                    err += dx -(rad<<1);
                }
            }
            imageView.setImage(writableImage);
        }
    }
}
