package sample;

public class bezier {
    private int p1X,p4X,p1Y,p4Y; //middle coordinate
    private int p2_leftX,p2_rightX, p3_leftX, p3_rightX; //outer coordinate
    private int p2_leftY,p2_rightY, p3_leftY, p3_rightY; //outer coordinate
    private int add_X,add_Y;
    //for heart shape
    public bezier(int x1, int y1, int x2, int y2) {//set p1 to p4
        this.add_X = (x2 - x1)/2;
        this.add_Y = (y2 - y1)/3;
        //middle
        this.p1X = x1 + add_X;
        this.p1Y = y1 + add_Y;
        this.p4X = x1 + add_X;
        this.p4Y = y2;
        //left
        this.p2_leftX = x1;
        this.p2_leftY = y1;
        this.p3_leftX = x1;
        this.p3_leftY = y2 - add_Y;
        //right
        this.p2_rightX = x2;
        this.p2_rightY = y1;
        this.p3_rightX = x2;
        this.p3_rightY = y2 - add_Y;
    }

    public double get_leftX(float t){
        float u = t/1000;
        float onu = 1-u;
        return p1X*Math.pow(onu,3) + 3*p2_leftX*u*onu*onu + 3*p3_leftX*u*u*onu + p4X*Math.pow(u,3);
    }
    public double get_leftY(float t){
        float u = t/1000;
        float onu = 1-u;
        return p1Y*Math.pow(onu,3) + 3*p2_leftY*u*onu*onu + 3*p3_leftY*u*u*onu + p4Y*Math.pow(u,3);
    }
    public double get_rightX(float t){
        float u = t/1000;
        float onu = 1-u;
        return p1X*Math.pow(onu,3) + 3*p2_rightX*u*onu*onu + 3*p3_rightX*u*u*onu + p4X*Math.pow(u,3);
    }
    public double get_rightY(float t){
        float u = t/1000;
        float onu = 1-u;
        return p1Y*Math.pow(onu,3) + 3*p2_rightY*u*onu*onu + 3*p3_rightY*u*u*onu + p4Y*Math.pow(u,3);
    }
}
