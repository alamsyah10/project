package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class Controller {
    @FXML
    private ImageView imageView;
    @FXML
    private Button btn_line,btn_circle,btn_heart;
    @FXML
    private TextField tf_x1,tf_y1,tf_x2,tf_y2,tf_xCircle,tf_yCircle,tf_rad;
    @FXML
    private Label lbl_error;
    public void initialize(){
        btn_line.setOnAction(this::makeLine);
        btn_circle.setOnAction(this::makecircle);
        btn_heart.setOnAction(this::makeheart);
    }

    private void makeLine(ActionEvent event) {
        imageView.setImage(null);
        lbl_error.setText(null);
        int x1 = Integer.parseInt(tf_x1.getText());
        int y1 = Integer.parseInt(tf_y1.getText());
        int x2 = Integer.parseInt(tf_x2.getText());
        int y2 = Integer.parseInt(tf_y2.getText());

        if (x1>=700 || x2>=700 || y1>=700 || y2>=700)
            lbl_error.setText("Out of bound");
        else{
            WritableImage writableImage = new WritableImage(700,700);
            PixelWriter pixelWriter = writableImage.getPixelWriter();
            int dx = x2-x1;
            int dy = y2-y1;
            int y;
            for (int x=x1;x<=x2;x++){
                y=y1+dy*(x-x1)/dx;
                pixelWriter.setColor(x,y, Color.BLACK);
                imageView.setImage(writableImage);
                //Thread.sleep(100); //bugged

            }
        }
    }
    private void makecircle(ActionEvent event){
        imageView.setImage(null);
        lbl_error.setText(null);
        int x0 = Integer.parseInt(tf_xCircle.getText());
        int y0 = Integer.parseInt(tf_yCircle.getText());
        int rad = Integer.parseInt(tf_rad.getText());

        if (x0+rad>=700 || y0+rad>=700)
            lbl_error.setText("Out of bound");
        else{
            WritableImage writableImage = new WritableImage(700,700);
            PixelWriter pixelWriter = writableImage.getPixelWriter();
            int x = rad -1;
            int y = 0;
            int dx = 1;
            int dy = 1;
            int err = dx - (rad<<1);

            while (x>=y){
                pixelWriter.setColor(x0 + x, y0 + y, Color.BLACK);
                pixelWriter.setColor(x0 + y, y0 + x, Color.BLACK);
                pixelWriter.setColor(x0 - y, y0 + x, Color.BLACK);
                pixelWriter.setColor(x0 - x, y0 + y, Color.BLACK);
                pixelWriter.setColor(x0 - x, y0 - y, Color.BLACK);
                pixelWriter.setColor(x0 - y, y0 - x, Color.BLACK);
                pixelWriter.setColor(x0 + y, y0 - x, Color.BLACK);
                pixelWriter.setColor(x0 + x, y0 - y, Color.BLACK);

                if (err<=0){
                    y++;
                    err += dy;
                    dy += 2;
                }
                else {
                    x--;
                    dx += 2;
                    err += dx -(rad<<1);
                }
            }
            imageView.setImage(writableImage);
        }
    }
    private void makeheart(ActionEvent event){
        imageView.setImage(null);
        lbl_error.setText(null);
        WritableImage writableImage = new WritableImage(700,700);
        PixelWriter pixelWriter = writableImage.getPixelWriter();
        int x1 = Integer.parseInt(tf_x1.getText());
        int y1 = Integer.parseInt(tf_y1.getText());
        int x2 = Integer.parseInt(tf_x2.getText());
        int y2 = Integer.parseInt(tf_y2.getText());
        bezier curve = new bezier(x1,y1,x2,y2);
        //left
        for (int i=0;i<=500;i++){
            int up_x = (int) curve.get_leftX(i);
            int up_y = (int) curve.get_leftY(i);
            //pixelWriter.setColor(up_x,up_y, Color.BLACK); //blackline
            int down_x = (int) curve.get_leftX(1000-i);
            int down_y = (int) curve.get_leftY(1000-i);
            //pixelWriter.setColor(down_x,down_y, Color.BLACK); //blackline
            if (up_x==down_x)
                for (int j = up_y;j<=down_y;j++)
                    pixelWriter.setColor(up_x,j,Color.RED);
        }
        //right
        for (int i=0;i<=500;i++){
            int up_x = (int) curve.get_rightX(i);
            int up_y = (int) curve.get_rightY(i);
            //pixelWriter.setColor(up_x,up_y, Color.BLACK); //blackline
            int down_x = (int) curve.get_rightX(1000-i);
            int down_y = (int) curve.get_rightY(1000-i);
            //pixelWriter.setColor(down_x,down_y, Color.BLACK); //blackline
            if (up_x==down_x)
                for (int j = up_y;j<=down_y;j++)
                    pixelWriter.setColor(up_x,j,Color.RED);
        }
        imageView.setImage(writableImage);
    }
}
