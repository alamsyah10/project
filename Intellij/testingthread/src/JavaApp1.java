class RunnableDemo implements Runnable {
    private Thread t;
    private String threadName;
    int a = 0;

    RunnableDemo(String threadName){
        this.threadName = threadName;
        System.out.println("Creating " + threadName);
    }

    public void run() {
        System.out.println("Running " + threadName);
        try {
            for (int i = 0; i < 99; i++) {
                a++;
                System.out.println("Nilai : " + a + ", " + threadName);
                Thread.sleep((int) (Math.random() * 4000) + 1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread " + threadName + " interupted");
        }
        System.out.println("Thread " + threadName + " exiting");
    }
    public void start(){
        System.out.println("Starting " + threadName);
        if (t==null){
            t = new Thread(this, threadName);
            t.start();
        }
    }
}

public class JavaApp1{
    public static void main(String arg[]){
        RunnableDemo runnableDemo1 = new RunnableDemo("Thread-1");
        runnableDemo1.start();

        RunnableDemo runnableDemo2 = new RunnableDemo("Thread-2");
        runnableDemo2.start();
    }
}
