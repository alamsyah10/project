package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

public class Controller {
    @FXML
    ImageView iv_real, iv_scaled, iv_zoomed;
    @FXML
    Button btn_zoom,btn_scale;
    @FXML
    TextField tf_zoom_x, tf_zoom_y, tf_zoom;
    @FXML
    TextField tf_scale_x, tf_scale_y;

    public void initialize(){
        real();
        btn_zoom.setOnAction(this::zoom);
        btn_scale.setOnAction(this::scale);
    }
    private void real(){
        Real image = new Real(825,750);
        image.draw();
        iv_real.setImage(image.result());
    }
    private void scale(ActionEvent event){
        System.out.println();
        System.out.println("Scale");
        int x = Integer.parseInt(tf_scale_x.getText());
        int y = Integer.parseInt(tf_scale_y.getText());
        Scale image = new Scale(x,y);
        image.draw();
        iv_scaled.setFitWidth(x);
        iv_scaled.setFitHeight(y);
        iv_scaled.setImage(image.result());
    }
    private void zoom(ActionEvent event){
        System.out.println();
        System.out.println("Zoom");
        int x = Integer.parseInt(tf_zoom_x.getText());
        int y = Integer.parseInt(tf_zoom_y.getText());
        int zoom = Integer.parseInt(tf_zoom.getText());
        Zoom image = new Zoom(825,750,zoom);
        image.draw(x,y,825,750);
        iv_zoomed.setImage(image.result());
    }


}
