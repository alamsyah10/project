package sample;

import javafx.scene.image.WritableImage;

public class Scale {
    private int width, height;
    WritableImage writableImage;

    public Scale(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void draw(){
        Real image = new Real(width,height);
        image.draw();
        writableImage = image.result();
    }

    public WritableImage result(){
        return writableImage;
    }
}
