package sample;

import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;

public class Zoom {
    private int width, height, zoom;
    private WritableImage newImage;


    public Zoom(int width, int height, int zoom) {
        this.width = width*zoom;
        this.height = height*zoom;
        this.zoom = zoom;
    }

    void draw(int x, int y, int width, int height) {
        Real old = new Real(this.width, this.height);
        old.draw();
        WritableImage oldImage = old.result();
        PixelReader reader = oldImage.getPixelReader();
        newImage = new WritableImage(reader, x * zoom, y * zoom, width, height);
    }

    WritableImage result() {
        return newImage;
    }
}
