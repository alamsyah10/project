package sample;

import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class Real{
    private int addX;
    private int addY;
    private int resX;
    private int resY;
    private int resMax;

    public Real(int resX, int resY) {
        this.resX = resX;
        this.resY = resY;
        this.addX = resX/3;
        this.addY = resY/4;
        this.resMax = Math.max(resX,resY);
    }

    private WritableImage writableImage;

    private int[] getpX(){
        int addX = this.addX;
        int[] pX = new int[4];
        for (int i=0;i<4;i++)
            pX[i] = i*addX;
        return pX;
    }
    private int[] getpY(int n){
        int addY = this.addY*n;
        int[] pY = new int[4];

        pY[0]=addY;
        pY[1]=addY-this.addY-1;
        pY[2]=addY+this.addY-1;
        pY[3]=addY;
        return pY;
    }

    void draw(){
        writableImage = new WritableImage(resX,resY);
        PixelWriter pixelWriter = this.writableImage.getPixelWriter();
        int[] pX = getpX();
        //draw merah
        int[] pY = getpY(1);
        Bezier curve = new Bezier(pX,pY,this.resX,this.resY);
        for(int i=0;i<this.resMax*2;i++){
            int X = (int) curve.getX(i);
            int Y = (int) curve.getY(i);
            for(int j=0;j<addY;j++)
                pixelWriter.setColor(X,Y+j, Color.RED);
        }
        //draw kuning
        pY = getpY(2);
        curve = new Bezier(pX,pY,this.resX,this.resY);
        for(int i=0;i<this.resMax*2;i++){
            int X = (int) curve.getX(i);
            int Y = (int) curve.getY(i);
//            System.out.print(X);
//            System.out.print(Y);
            for(int j=0;j<addY;j++)
                pixelWriter.setColor(X,Y+j, Color.YELLOW);
        }
        for (int i=0;i<4;i++)
            System.out.print(pX[i]+" ");
    }

    WritableImage result(){
        return this.writableImage;
    }
}
