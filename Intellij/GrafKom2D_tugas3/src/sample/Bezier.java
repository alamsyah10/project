package sample;

public class Bezier {
    private int[] pX= new int[4];
    private int[] pY= new int[4];
    private int resX,resY,resMax;

    public Bezier(int[] pX,int[] pY){
        this.pX = pX;
        this.pY = pY;
    }

    public Bezier(int[] pX, int[] pY, int resX, int resY) {
        this.pX = pX;
        this.pY = pY;
        this.resX = resX;
        this.resY = resY;
        this.resMax = Math.max(resX,resY);
        for (int i=0;i<4;i++)
            System.out.print(pX[i]+" ");
    }

    public int[] getpX() {
        return pX;
    }

    public int[] getpY() {
        return pY;
    }

    public int getResX() {
        return resX;
    }

    public int getResY() {
        return resY;
    }

    public int getResMax() {
        return resMax;
    }

    public double getX(float t /* resolution*2 */){
        float u = t/(this.resMax*2);
//        System.out.print(t);
        float onu = 1-u;
        System.out.print(u + " ");
        //System.out.print(pX[0]*Math.pow(onu,3) + 3*pX[1]*u*onu*onu + 3*pX[2]*u*u*onu + pX[3]*Math.pow(u,3) + " " + onu + " " + resMax);
        return pX[0]*Math.pow(onu,3) + 3*pX[1]*u*onu*onu + 3*pX[2]*u*u*onu + pX[3]*Math.pow(u,3);
    }
    public double getY(float t /* resolution*2 */){
        float u = t/(this.resMax*2);
        float onu = 1-u;
        return pY[0]*Math.pow(onu,3) + 3*pY[1]*u*onu*onu + 3*pY[2]*u*u*onu + pY[3]*Math.pow(u,3);
    }
}
