import java.util.concurrent.Semaphore;
import java.lang.Thread;

public class Laundromat {
    public static void main(String Arg){
        Semaphore washer = new Semaphore(30);
        Semaphore dryer = new Semaphore(15);

        MyThread[] myThreads = new MyThread[50];
        for (int i=0; i<50;i++){
            myThreads[i]=new MyThread(washer,dryer,i);
            myThreads[i].start();

        }
    }
}

class Shared {
    static int usableWasher = 30;
    static int usableDryer = 15;
}
class MyThread extends Thread {
    private Semaphore washer;
    private Semaphore dryer;
    private int customer;
    private Thread thread;

    public MyThread(Semaphore washer, Semaphore dryer, int customer) {
        this.washer = washer;
        this.dryer = dryer;
        this.customer = customer;
    }

    public void run() {

        try {
            System.out.println("Customer " + customer + " in.");
            Shared.usableWasher--;
            washer.acquire();
            System.out.println("Customer " + customer + " washing");
            Thread.sleep(50);
            washer.release();
            Shared.usableWasher++;

            System.out.println("Customer " + customer + " in again");
            Shared.usableDryer--;
            dryer.acquire();
            System.out.println("Customer " + customer + " drying");
            Thread.sleep(40);
            dryer.release();
            Shared.usableDryer++;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}